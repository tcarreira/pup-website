+++
title = "E103 Pote Coiote"
itunes_title = "Pote Coiote"
episode = 103
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e103/e103.mp3"
podcast_duration = "0:57:00"
podcast_bytes = "55188122"
author = "Podcast Ubuntu Portugal"
date = "2020-08-13"
description = "Essas férias? Nós cá continuamos, mais uma rodada que começou menos inspirada que o habitual, mas que depois se revelou que efectivamente era menos inspirada que o habitual. Ainda assim, foi um bonito episódio."
thumbnail = "images/e103.png"

featured = false
categories = ["Episódio"]
tags = [
  "lvgl",
  "wayland",
  "pixel",
  "apple",
  "lomiri",
  "Ubuntu",
  "LibreTrend",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e103", "E103"]
+++

Essas férias? Nós cá continuamos, mais uma rodada que começou menos inspirada que o habitual, mas que depois se revelou que efectivamente era menos inspirada que o habitual. Ainda assim, foi um bonito episódio.
Já sabem: oiçam, subscrevam e partilhem!

* https://github.com/lupyuen/lvgl-wayland
* https://lupyuen.github.io/pinetime-rust-mynewt/articles/wayland
* https://twitter.com/fredldotme/status/1284134511752679424
* https://forums.ubports.com/topic/4621/google-pixel-3a-sargo
* https://twitter.com/costalesdev/status/1287369606068408321
* https://www.xda-developers.com/safetynet-hardware-attestation-hide-root-magisk
* https://developer.apple.com/wwdc20/


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

