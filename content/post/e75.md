+++
title = "E75 Saudades do UBPorts?"
itunes_title = "Saudades do UBPorts?"
episode = 75
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e75/e075.mp3"
podcast_duration = "0:55:55"
podcast_bytes = "53747873"
author = "Podcast Ubuntu Portugal"
date = "2020-01-30"
description = "Saudades do UBPorts? Preparados para mais 57 minutos na companhia das 2 vozes mais sensuais no universo de todos os podcasts sobre Ubuntu falados na língua de Camões? Já sabem: oiçam, comentem e partilhem!"
thumbnail = "images/e75.png"

featured = false
categories = ["Episódio"]
tags = [
  "streetcomplete",
  "newpipe",
  "opendataday",
  "ubports",
  "anbox",
  "Wildebeest",
  "WikiCon",
  "fosdem",
  "pixelscamp",
  "survey",
  "HumbleBundle",
  "LibreTrend",
  "Ubuntu",
]
seasons = ["S01"]
aliases = ["e75", "E75"]
+++

Saudades do UBPorts? Preparados para mais 57 minutos na companhia das 2 vozes mais sensuais no universo de todos os podcasts sobre Ubuntu falados na língua de Camões? Já sabem: oiçam, comentem e partilhem!

* https://wiki.openstreetmap.org/wiki/StreetComplete
* https://f-droid.org/en/packages/de.westnordost.streetcomplete/
* https://f-droid.org/en/packages/org.schabi.newpipe/
* https://opendataday.org/
* https://meta.wikimedia.org/wiki/WikiCon_Portugal/Programa
* https://mobile.twitter.com/RealDanct12/status/1217100933567401986
* https://ubuntu.com/blog/canonical-introduces-anbox-cloud-scalable-android-in-the-cloud
* https://anbox-cloud.io/
* https://fosdem.org
* https://www.humblebundle.com/software/data-science-and-machine-learning-software?partner=PUP
* https://www.humblebundle.com/books/artificial-intelligence-mit-press-books?partner=PUP


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

