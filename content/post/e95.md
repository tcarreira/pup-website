+++
title = "E95 Os melhores Santos"
itunes_title = "Os melhores Santos"
episode = 95
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e95/e095.mp3"
podcast_duration = "0:57:00"
podcast_bytes = "55182688"
author = "Podcast Ubuntu Portugal"
date = "2020-06-20"
description = "Uma inédita performance artística, o drama dos snaps no Mint, as aventuras com self-hosting de Jitsi Meet, o cantinho da Impressão 3D, colaboração com Markdown, um primeiro olhar para o Pine64 PinePhone UBports Community Edition, a pré-venda dos PineTab com Ubuntu Touch. Ainda passámos pelas próximas actividades da comunidade Ubuntu-PT, traduções de Software Livre, LXD e containers com reverse Proxy, e pelas notícias do UBports e do seu ecossistema incluindo clickable e Halium e os avanços que se avizinham para o VollaPhone."
thumbnail = "images/e95.png"

featured = false
categories = ["Episódio"]
tags = [
  "Linux Mint",
  "Snapd",
  "Chromium",
  "deb",
  "dpkg",
  "apt",
  "Jitsi",
  "Jibri",
  "Jitsi Meet",
  "streaming",
  "Blender",
  "Okular",
  "Markdown",
  "VSCode",
  "VSCodium",
  "Pine64",
  "PinePhone",
  "UBports",
  "Ubuntu Touch",
  "PineTab",
  "Ubuntu-PT",
  "Traduções",
  "Wordpress",
  "FSF Europe",
  "LXD",
  "UBports Q&A",
  "Clickable",
  "Halium",
  "Volla",
  "VollaPhone",
  "LibreTrend",
  "Librebox",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e95", "E95"]
+++

Uma inédita performance artística, o drama dos snaps no Mint, as aventuras com self-hosting de Jitsi Meet, o cantinho da Impressão 3D, colaboração com Markdown, um primeiro olhar para o Pine64 PinePhone UBports Community Edition, a pré-venda dos PineTab com Ubuntu Touch. Ainda passámos pelas próximas actividades da comunidade Ubuntu-PT, traduções de Software Livre, LXD e containers com reverse Proxy, e pelas notícias do UBports e do seu ecossistema incluindo clickable e Halium e os avanços que se avizinham para o VollaPhone.
Já sabem: oiçam, subscrevam e partilhem!

* https://libretrend.com/specs/librebox


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

