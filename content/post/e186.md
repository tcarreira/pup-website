+++
title = "E186 Hoje há pombo correio"
itunes_title = "Hoje há pombo correio"
episode = 186
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e186/e186.mp3"
podcast_duration = "0:58:09"
podcast_bytes = "56128926"
author = "Podcast Ubuntu Portugal"
date = "2022-03-17"
description = "O Raspberry Pi faz 10 anos, o Diogo já tem conta no archive.org, o Carrondo descobriu que zero também paga IVA e o Miguel não conseguiu escavar um bunker. Tudo isto enquanto tentámos formar uma equipa de 0AD experimentando uma extensão Gnome para ajudar na gestão dos snaps…"
thumbnail = "images/e186.png"

featured = false
categories = ["Episódio"]
tags = [
  "pi",
  "0ad",
  "ctt",
  "snaps",
  "jingpad",
  "codium",
  "ubuntu",
  "Collabora",
  "LibreTrend",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e186", "E186"]
+++

O Raspberry Pi faz 10 anos, o Diogo já tem conta no archive.org, o Carrondo descobriu que zero também paga IVA e o Miguel não conseguiu escavar um bunker. Tudo isto enquanto tentámos formar uma equipa de 0AD experimentando uma extensão Gnome para ajudar na gestão dos snaps…
Já sabem: oiçam, subscrevam e partilhem!

* https://www.navidrome.org/
* https://github.com/MFDGaming/ubuntu-in-termux
* https://archive.org/details/@diogoconstantino
* https://www.youtube.com/c/PodcastUbuntuPortugal/channels
* https://www.youtube.com/user/play0ad
* https://www.youtube.com/channel/UC5Sf1aQufzzWATg9TJzg7mQ
* https://www.youtube.com/channel/UCS-SFei6NFRuGN8CKtAsYrA
* https://web.archive.org/web/20220313192430/https://www.debugpoint.com/2021/10/manual-installation-gnome-extension/
* https://web.archive.org/web/20220312202759/https://ubuntu-web.org/blog/ubuntu-web-20.04.4/
* https://twitter.com/fredldotme/status/1503007311182372867
* https://twitter.com/scas/status/1503009070432890884
* https://www.youtube.com/watch?v=eiwm5TMHIy8
* https://www.youtube.com/watch?v=rS9CbsohFGk
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

