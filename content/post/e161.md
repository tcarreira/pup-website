+++
title = "E161 KISS"
itunes_title = "KISS"
episode = 161
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e161/e161.mp3"
podcast_duration = "0:53:48"
podcast_bytes = "52108019"
author = "Podcast Ubuntu Portugal"
date = "2021-09-23"
description = "O Diogo andou, finalmente, a simplificar o seu setup, reduzindo drasticamente a quantidade de equipamento que usa, não perdendo nada em produtividade, potenciando a eficácia…O Carrondo andou a meter as mãos no matrix!"
thumbnail = "images/e161.png"

featured = false
categories = ["Episódio"]
tags = [
  "setup",
  "matrix",
  "lxd",
  "docker",
  "ubuntu",
  "LibreTrend",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e161", "E161"]
+++

O Diogo andou, finalmente, a simplificar o seu setup, reduzindo drasticamente a quantidade de equipamento que usa, não perdendo nada em produtividade, potenciando a eficácia…O Carrondo andou a meter as mãos no matrix!
Já sabem: oiçam, subscrevam e partilhem!

* https://enterpriselinuxsecurity.show/
* https://keychronwireless.referralcandy.com/3P2MKM7
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal



### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

