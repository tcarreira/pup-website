+++
title = "E30 Mestres no Open Source"
itunes_title = "Mestres no Open Source"
episode = 30
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e30/e030.mp3"
podcast_duration = "0:57:00"
podcast_bytes = "55109669"
author = "Podcast Ubuntu Portugal"
date = "2018-10-20"
description = "Revimos o valioso feedback dos ouvintes, voltámos a falar do Pinebook, Olimex e Pihole. Depois passámos apelos à participação da comunidade, mas o prato principal foi a entrevista à Professora Manuela Aparício do Mestrado em Open Source Software do ISCTE/IUL. No fim além da agenda ainda fizemos uma pequena digressão pelo Open Source Lisboa 2018."
thumbnail = "images/e30.png"

featured = false
categories = ["Episódio"]
tags = [
  "ubuntu",
  "ubports",
  "lubuntu",
  "cosmic",
  "cuttlefish",
  "olimex",
  "pinebook",
  "moss",
]
seasons = ["S01"]
aliases = ["e30", "E30"]
+++

Revimos o valioso feedback dos ouvintes, voltámos a falar do Pinebook, Olimex e Pihole. Depois passámos apelos à participação da comunidade, mas o prato principal foi a entrevista à Professora Manuela Aparício do Mestrado em Open Source Software do ISCTE/IUL. No fim além da agenda ainda fizemos uma pequena digressão pelo Open Source Lisboa 2018.

* https://lists.ubuntu.com/archives/lubuntu-devel/2018-September/001351.html
* https://twitter.com/LubuntuOfficial/status/1045370501667803141
* https://twitter.com/LubuntuOfficial/status/1045345732465573888
* https://lists.ubuntu.com/archives/lubuntu-devel/2018-September/001354.html
* https://discourse.ubuntu.com/t/quick-test/8144
* https://discourse.ubuntu.com/t/survey-ubuntu-session-vs-vanilla-gnome-what-do-you-want-ubuntu-to-evolve-to/8133
* https://community.ubuntu.com/t/mockups-new-design-discussions/1898/1
* https://discourse.ubuntu.com/t/call-for-testing-nvidia-prime-in-ubuntu-18-04-and-18-10/8207
* https://www.omgubuntu.co.uk/2018/09/ubuntu-18-10-beta-download-out
* https://www.iscte-iul.pt/curso/60/mestrado-software-de-codigo-aberto
* https://ciencia.iscte-iul.pt/authors/manuela-aparicio/cv


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

