+++
title = "E141 Soberania Digital"
itunes_title = "Soberania Digital"
episode = 141
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e141/e141.mp3"
podcast_duration = "0:57:00"
podcast_bytes = "55183107"
author = "Podcast Ubuntu Portugal"
date = "2021-05-06"
description = "Algumas dicas sobre cursos de formação de programação, esclarecimento de dúvidas em tempo real, de patronos e um balanço do último encontro da LoCo Ubuntu Portugal foram os tópicos destes 57 minutos de amena cavaqueira no vosso podcast preferido."
thumbnail = "images/e141.png"

featured = false
categories = ["Episódio"]
tags = [
  "encontro",
  "tradução",
  "mate",
  "programação",
  "formação",
  "21.04",
  "16.04",
  "end-of-life",
  "ubports",
  "ubuntu",
  "LibreTrend",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e141", "E141"]
+++

Algumas dicas sobre cursos de formação de programação, esclarecimento de dúvidas em tempo real, de patronos e um balanço do último encontro da LoCo Ubuntu Portugal foram os tópicos destes 57 minutos de amena cavaqueira no vosso podcast preferido.
Já sabem: oiçam, subscrevam e partilhem!

* https://www.youtube.com/watch?v=0R7_zQh7RmI
* https://www.freecodecamp.org/
* https://www.codecademy.com/
* https://acloudguru.com/
* https://wiki.ubuntu.com/luks-management
* https://ubuntu.com/blog/ubuntu-21-04-is-here
* https://discourse.ubuntu.com/t/hirsute-hippo-release-notes/19221
* https://ubuntu.com/16-04
* https://www.gofundme.com/f/anbox-on-ut
* https://keychronwireless.referralcandy.com/3P2MKM7
* https://www.humblebundle.com/books/head-first-programming-oreilly-books?parner=PUP
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal



### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

