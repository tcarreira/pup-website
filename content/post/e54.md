+++
title = "E54 Disco com o Dingo"
itunes_title = "Disco com o Dingo"
episode = 54
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e54/e054.mp3"
podcast_duration = "1:02:39"
podcast_bytes = "90239058"
author = "Podcast Ubuntu Portugal"
date = "2019-04-21"
description = "As nossas aventuras, falámos do Ubuntu 19.04 Disco Dingo e da Ubucon Portugal 2019. Mas claro que também passámos pelas notícias, por alguns snaps e pela agenda."
thumbnail = "images/e54.png"

featured = false
categories = ["Episódio"]
tags = [
  "NOT YET",
]
seasons = ["S01"]
aliases = ["e54", "E54"]
+++

As nossas aventuras, falámos do Ubuntu 19.04 Disco Dingo e da Ubucon Portugal 2019. Mas claro que também passámos pelas notícias, por alguns snaps e pela agenda.
Já sabes: ouve, subscreve e partilha!

* https://www.youtube.com/watch?v=aJWSE7acl-Q
* https://ubports.com/pt_PT/blog/ubports-blog-1/post/interview-with-jan-sprinz-217
* https://matrix.org/blog/2019/04/11/security-incident/
* https://snapcraft.io/mailspring
* https://snapcraft.io/search?q=rambox
* http://loco.ubuntu.com/events/ubuntu-pt/3825-encontro-ubuntu-pt-sintra-lan%C3%A7amento-disco-dingo/
* https://ubuntu-paris.org/
* https://summit.creativecommons.org/


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

