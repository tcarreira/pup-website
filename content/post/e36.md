+++
title = "E36 Alta Coltura"
itunes_title = "Alta Coltura"
episode = 36
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e36/e036.mp3"
podcast_duration = "1:03:31"
podcast_bytes = "71311579"
author = "Podcast Ubuntu Portugal"
date = "2018-11-18"
description = "Esta semana o trio maravilha dedicou a sua atenção a sugestões de leitura, técnica ou não, porque a vida não são só podcasts… As novidades no mundo SolusOS, a parceria da Canonical e da Samsung e o projecto Linux on Dex, sem esquecer a Festa do Software Livre da Moita 2018, que está já aí! Já sabes: Ouve, subscreve e partilha!"
thumbnail = "images/e36.png"

featured = false
categories = ["Episódio"]
tags = [
  "livros",
  "devops",
  "humble bundle",
  "steam",
  "oni",
  "vfat",
  "moita",
  "software livre moita 2018",
  "steam",
  "protondb",
  "pinebook",
  "solus",
  "wireguard",
  "kernel",
  "software freedom law center",
  "thunderbird",
  "firefox",
  "privacidade",
  "tracking",
  "cookies",
  "samsung",
  "dex",
  "protondb",
]
seasons = ["S01"]
aliases = ["e36", "E36"]
+++

Esta semana o trio maravilha dedicou a sua atenção a sugestões de leitura, técnica ou não, porque a vida não são só podcasts… As novidades no mundo SolusOS, a parceria da Canonical e da Samsung e o projecto Linux on Dex, sem esquecer a Festa do Software Livre da Moita 2018, que está já aí! Já sabes: Ouve, subscreve e partilha!

* https://www.humblebundle.com/books/dev-ops-oreilly
* https://blog.mozilla.org/thunderbird/2017/05/thunderbirds-future-home/
* https://www.phoronix.com/scan.php?page=news_item&px=Solus-Open-Letter
* https://www.phoronix.com/scan.php?page=news_item&px=WireGuard-Not-In-4.20
* https://twitter.com/sflc/status/1058386456115326976
* https://twitter.com/sflc/status/1058388361449259009
* https://www.mozilla.org/en-US/firefox/63.0/releasenotes/
* https://www.linuxondex.com/
* https://www.protondb.com


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

