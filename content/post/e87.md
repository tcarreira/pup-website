+++
title = "E87 Festa Confinada"
itunes_title = "Festa Confinada"
episode = 87
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e87/e087.mp3"
podcast_duration = "0:57:00"
podcast_bytes = "56276220"
author = "Podcast Ubuntu Portugal"
date = "2020-04-23"
description = "O Diogo continua com as suas aventura de redes e conta-nos como é o seu gateway ideal. Tiago continua focado na impressão 3D."
thumbnail = "images/e87.png"

featured = false
categories = ["Episódio"]
tags = [
  "Redes",
  "Impressão 3D",
  "Mir",
  "UBports",
  "Ubuntu Touch",
  "Lomiri",
  "OTA-12",
  "FocalFossa",
  "Ubuntu",
  "SuperTuxKart",
  "VSCodium",
  "VSCode",
]
seasons = ["S01"]
aliases = ["e87", "E87"]
+++

O Diogo continua com as suas aventura de redes e conta-nos como é o seu gateway ideal. Tiago continua focado na impressão 3D.
Há muitas notícias de releases, o Mir 1.8.0, a OTA-12 do Ubuntu Touch está em fase de testes e o Ubuntu 20.04 sai já dia 23 e claro isto é motivo de festa. Não percas o episódio 87!!!
Ouve! Subscreve! Partilha!

* https://www.omgubuntu.co.uk/2019/10/ubuntu-20-04-release-features
* https://www.omgubuntu.co.uk/2020/04/ubuntu-20-04-screenshot-tour
* https://discourse.ubuntu.com/t/mir-1-8-0-release/15351
* https://ubports.com/pt_PT/blog/ubports-blog-1/post/call-for-testing-ubuntu-touch-ota-12-273
* https://forums.ubports.com/topic/4113/ota-12-call-for-testing


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

