+++
title = "E12 UmaHub"
itunes_title = "UmaHub"
episode = 12
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e12/e012.mp3"
podcast_duration = "0:36:56"
podcast_bytes = "17730456"
author = "Podcast Ubuntu Portugal"
date = "2017-12-21"
description = "Uma Hub"
thumbnail = "images/e12.png"

featured = false
categories = ["Episódio"]
tags = [
  "NOT YET",
]
seasons = ["S00"]
aliases = ["e12", "E12"]
+++

Neste episódio do Podcast Ubuntu Portugal vamos falar sobre a nossa [LoCo](http://loco.ubuntu.com/teams/ubuntu-pt), [UBPorts](https://ubports.com) (claro!) e o projecto [UmaHub](http://umahub.co) entrevistando a sua mentora [Elena Kolevska](http://www.elenakolevska.com).

## Notas do episódio

* https://ubuntu-pt.org/2017/12/estado-de-saude-da-comunidade-revalidada/
* https://ubuntu-pt.org/2017/12/foi-renovado-o-conselho-da-comunidade-portuguesa/
* http://www.omgubuntu.co.uk/2017/12/ubuntu-corrupting-lenovo-laptop-bios
* https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1734147
* https://github.com/ubports/ubuntu-touch/pulls?q=is%3Aopen+is%3Apr+milestone%3A%2215.04+OTA-3%22
* https://github.com/ubports/ubuntu-touch/pulls?q=is%3Aopen+is%3Apr+milestone%3A%2216.04+OTA-4%22
* https://insights.ubuntu.com/2017/12/14/ubucon-europe-27th-28th-29th-of-april-2018-in-gijon-xixon-spain/
* http://loco.ubuntu.com/events/ubuntu-pt/3667-encontro-ubuntu-pt-sintra/

Comenta este episódio no nosso novo [sub-reddit](https://www.reddit.com/r/PodcastUbuntuPortugal/)!

## Comunidade Ubuntu Portugal

### Na web
* https://ubuntu-pt.org
* http://loco.ubuntu.com/teams/ubuntu-pt/events/

### No telegram
* https://t.me/ubuntuptgeral
* https://t.me/ubuntuportugal

### No facebook
* https://www.facebook.com/ubuntuportugal
* https://www.facebook.com/groups/ubuntupt/

### No Twitter
* https://twitter.com/ubuntuportugal

### No Google+:
* https://plus.google.com/u/0/communities/117643052314062774319

## Podcast Ubuntu Portugal
Subscrevam a feed utilizando o vosso Podcatcher ou a pocasts app da Apple.
Se não encontrarem nessas aplicações podem sempre usar directamente o feeed:
* https://ubuntu-pt.org/podcast

Também nos podem seguir…

### No Telegram
* https://t.me/PodcastUbuntuPortugal

### No facebook
* https://www.facebook.com/podcastubuntuportugal/

### SoundCloud
* https://soundcloud.com/user-417426793

### MixCloud
* https://www.mixcloud.com/PodcastUbuntuPortugal/


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

