+++
title = "E16 Porta-me, por favor"
itunes_title = "Porta-me, por favor"
episode = 16
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e16/e016.mp3"
podcast_duration = "0:48:30"
podcast_bytes = "23287919"
author = "Podcast Ubuntu Portugal"
date = "2018-02-20"
description = "Porta-me, por favor!"
thumbnail = "images/e16.png"

featured = false
categories = ["Episódio"]
tags = [
  "NOT YET",
]
seasons = ["S00"]
aliases = ["e16", "E16"]
+++

Muitos upgrades fantásticos, os melhores eventos e mergulhar um pouco na comunidade UBports é o que temos para este episódio.

* https://telegram.org/blog/tdlib
* https://insights.ubuntu.com/2018/01/26/a-peek-at-the-snapcraft-summit
* https://insights.ubuntu.com/2018/02/01/snapcraft-summit-summary-day-1
* https://insights.ubuntu.com/2018/02/01/snapcraft-summit-summary-day-2
* https://insights.ubuntu.com/2018/02/02/snapcraft-summit-summary-day-3
* https://insights.ubuntu.com/2018/02/03/snapcraft-summit-summary-day-5
* https://fosdem.org
* https://www.winehq.org/announce/3.0
* https://www.videolan.org/vlc/releases/3.0.0.html
* https://youtu.be/YHBve8v13VY
* https://nextcloud.com/blog/nextcloud-13-brings-secure-file-sync-and-collaboration-to-the-next-level/
* http://ubucon.org/en/events/ubucon-europe/schedule/

## Comunidade Ubuntu Portugal

### Na web
* https://ubuntu-pt.org
* http://loco.ubuntu.com/teams/ubuntu-pt/events/

### No telegram
* https://t.me/ubuntuptgeral
* https://t.me/ubuntuportugal

### No facebook
* https://www.facebook.com/ubuntuportugal
* https://www.facebook.com/groups/ubuntupt/

### No Twitter
* https://twitter.com/ubuntuportugal

### No Google+:
* https://plus.google.com/u/0/communities/117643052314062774319

## Podcast Ubuntu Portugal
Subscrevam a feed utilizando o vosso Podcatcher ou a pocasts app da Apple.
Se não encontrarem nessas aplicações podem sempre usar directamente o feeed:
* https://ubuntu-pt.org/podcast

Também nos podem seguir…

### No Telegram
* https://t.me/PodcastUbuntuPortugal

### No facebook
* https://www.facebook.com/podcastubuntuportugal/

### SoundCloud
* https://soundcloud.com/user-417426793


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

