+++
title = "E230 Tu tens um evento!"
itunes_title = "Tu tens um evento!"
episode = 230
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e230/e230.mp3"
podcast_duration = "0:56:18"
podcast_bytes = "81092021"
author = "Podcast Ubuntu Portugal"
date = "2023-01-19"
description = "Mais um típico episódio cheio de outras cenas e com algum Ubuntu!"
thumbnail = "images/e230.png"

featured = false
categories = ["Episódio"]
tags = [
  "previsões",
  "Ubuntu",
  "Collabora",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e230", "E230"]
+++

Dizem que Janeiro é o mês mais triste do ano! Até pode ser para muita gente mas não é certamente para a comunidade Ubuntu Portugal e basta olhar para a agenda deste mês para perceber que em Aveiro, depois em Sintra e continuando em Lisboa o que não falta é festa e animação. Olhando para o início de Fevereiro parece que a história se repete mas depois se verá!
Já sabem: oiçam, subscrevam e partilhem!

* https://discourse.ubuntu.com/t/lunar-lobster-23-04-wallpaper-competition/33132
* https://wiki.ubuntu.com/UbuntuFreeCultureShowcase#preview
* https://discourse.ubuntu.com/t/announcing-edubuntu-revival/32929
* https://www.youtube.com/watch?v=qmlabKbpTGM
* https://web.archive.org/web/20230110202417/https://ubports.com/pt/blog/ubports-news-19/post/devices-website-updated-3874
* https://web.archive.org/web/20230110202218/https://ubports.com/pt/blog/from-the-ubports-newsletter-team-15/post/your-last-ubports-newsletter-of-2022-happy-new-year-3875
* https://www.humblebundle.com/software/anyone-can-learn-to-code-complete-2023-online-course-software?partner=PUP
* https://www.humblebundle.com/books/linux-and-sysadmin-tools-oreilly-books?partner=PUP
* https://loco.ubuntu.com/events/ubuntu-pt/4308-encontro-ubuntu-pt-aveiro/
* https://loco.ubuntu.com/events/ubuntu-pt/4309-encontro-ubuntu-pt-janeiro-sintra/
* https://loco.ubuntu.com/events/ubuntu-pt/4310-workshop-de-introdu%C3%A7%C3%A3o-ao-scratch/
* https://loco.ubuntu.com/teams/ubuntu-pt/
* https://fosdem.org/
* https://pt2023.mini.debconf.org/
* https://shop.nitrokey.com/shop?aff_ref=3
* https://masto.pt/@pup
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

