+++
type = "Event"
date = 2023-01-28
title = "Workshop de Introdução ao Scratch"
tags = ['evento', 'cal-ubuntu-pt']
event_url = "https://loco.ubuntu.com/events/ubuntu-pt/4310-workshop-de-introdu%C3%A7%C3%A3o-ao-scratch/"
+++
Vamos aprender a criar um jogo?

Utilizando o ambiente de programação Scratch, podemos aprender a programar e criar os nossos próprios projetos. Aqui, criar um jogo é muito divertido!

O Scratch …

… é a ferramenta ideal para os mais jóvens (mas não só), introduzirem-se à programação de uma forma divertida por permitir rápida e facilmente obter resultados visuais.

Com o Scratch é fácil criar estórias, jogos e animações e partilhar o resultado com todo o mundo!

O Scratch é uma linguagem de programação educacional criada inicialmente no MIT Media Lab, e agora é gerido pela Fundação Scratch e é ideal para crianças dos 8 aos 16, mas também é utilizado em cursos de instituições de ensino superior para uma primeira introdução à programação.

Mais detalhes e inscrições (obrigatórias) na página do Centro Linux em: https://scratch.centrolinux.pt


Localização:

MILL – Makers In Little Lisbon
Calçada do Moinho de Vento, 14B,
1150-236 Lisboa

mapa: https://osm.org/go/b5crq_xVM?layers=N