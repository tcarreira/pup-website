+++
type = "Event"
date = 2022-11-24
title = "Encontro Ubuntu-pt @ Sintra"
tags = ['evento', 'cal-ubuntu-pt']
event_url = "https://loco.ubuntu.com/events/ubuntu-pt/4261-encontro-ubuntu-pt-sintra/"
+++
Todos os meses, numa quinta-feira, a comunidade Ubuntu Portugal reúne-se no Saloon, em Sintra.

Vem, traz um amigo ou um familiar e vem conviver e partilhar experiências com o resto da comunidade portuguesa..

Mais Informações:
Saloon
Avenida Movimento das Forças Armadas n 5, 2710-433 Sintra
(2 min a pé da estaçao de comboios da portela de Sintra)
O wi-fi é grátis.