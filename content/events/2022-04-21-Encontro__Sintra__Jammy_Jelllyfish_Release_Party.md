+++
type = "Event"
date = 2022-04-21
title = "Encontro @ Sintra + Jammy Jelllyfish Release Party"
tags = ['evento', 'cal-ubuntu-pt']
event_url = "https://loco.ubuntu.com/events/ubuntu-pt/4249-encontro-sintra-jammy-jelllyfish-release-party/"
+++
Todos os meses, numa quinta-feira, a comunidade Ubuntu Portugal reúne-se no Bar Saloon, em Sintra, para um encontro social a partidas as 20h.

Este mês fazemos também uma festa de lançamento da nova versão LTS do Ubuntu, o 22.04 Jammy Jellyfish.

Vem, traz um amigo ou um familiar e vem conviver e partilhar experiências com a comunidade.

Mais Informações:
Saloon
Avenida Movimento das Forças Armadas n 5, 2710-433 Sintra
(2 min a pé da estaçao de comboios da portela de Sintra)
O wi-fi é grátis.
http://www.openstreetmap.org/node/1594158358