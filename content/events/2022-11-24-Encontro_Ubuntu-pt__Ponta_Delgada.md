+++
type = "Event"
date = 2022-11-24
title = "Encontro Ubuntu-pt @ Ponta Delgada"
tags = ['evento', 'cal-ubuntu-pt']
event_url = "https://loco.ubuntu.com/events/ubuntu-pt/4262-encontro-ubuntu-pt-ponta-delgada/"
+++
De vez em quando, numa quinta-feira, a comunidade Ubuntu Portugal reúne-se em Ponta Delgada, para jantar e conversar.

Vem, traz um amigo ou um familiar e vem conviver e partilhar experiências com o resto da comunidade portuguesa...

(Por ser um jantar, se quiseres garantir o teu lugar à mesa convém avisares que vais, para isso usa o nosso telegram: https://t.me/ubuntuptgeral)