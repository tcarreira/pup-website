+++
type = "Event"
date = 2022-01-27
title = "Encontro Mensal - olhar antecipado para o Jammy"
tags = ['evento', 'cal-ubuntu-pt']
event_url = "https://loco.ubuntu.com/events/ubuntu-pt/4244-encontro-mensal-olhar-antecipado-para-o-jammy/"
+++
Este mês voltamos aos eventos online, e vamos aproveitar para espreitar o actual estado de desenvolvimento da próxima LTS do Ubuntu o Jammy Jellyfish.

Venham daí, juntem-se a nós em https://quentinho.ubcasts.com/JammyJaneiro
a partir das 21h (UTC/Lisboa).