+++
type = "Event"
date = 2022-10-20
title = "Encontro mensal Ubuntu-pt @ Sintra & Kinetic Kudu release party"
tags = ['evento', 'cal-ubuntu-pt']
event_url = "https://loco.ubuntu.com/events/ubuntu-pt/4257-encontro-mensal-ubuntu-pt-sintra-kinetic-kudu-release-party/"
+++
Todos os meses, numa quinta-feira, a comunidade Ubuntu Portugal reúne-se no Bar Saloon, em Sintra, para um encontro social.

Este mês assinalamos também o lançamento do Ubuntu 22.10 Kinetic Kudu

Vem, traz um amigo ou um familiar e vem conviver e partilhar experiências com a comunidade.

Mais Informações:
Saloon
Avenida Movimento das Forças Armadas n 5, 2710-433 Sintra
(2 min a pé da estaçao de comboios da portela de Sintra)
O wi-fi é grátis.
http://www.openstreetmap.org/node/1594158358