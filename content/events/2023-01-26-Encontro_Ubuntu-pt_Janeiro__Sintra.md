+++
type = "Event"
date = 2023-01-26
title = "Encontro Ubuntu-pt Janeiro @ Sintra"
tags = ['evento', 'cal-ubuntu-pt']
event_url = "https://loco.ubuntu.com/events/ubuntu-pt/4309-encontro-ubuntu-pt-janeiro-sintra/"
+++
Todos os meses, numa quinta-feira, a comunidade Ubuntu Portugal encontra-se no Saloon, em Sintra.

Vem, traz um amigo ou um familiar e vem conviver e partilhar experiências com o resto da comunidade portuguesa..

Mais Informações:
Saloon
Avenida Movimento das Forças Armadas n 5, 2710-433 Sintra
(2 min a pé da estaçao de comboios da portela de Sintra)
http://www.openstreetmap.org/node/1594158358

O wi-fi é grátis!

