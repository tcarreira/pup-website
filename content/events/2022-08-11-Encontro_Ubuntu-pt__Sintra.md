+++
type = "Event"
date = 2022-08-11
title = "Encontro Ubuntu-pt @ Sintra"
tags = ['evento', 'cal-ubuntu-pt']
event_url = "https://loco.ubuntu.com/events/ubuntu-pt/4255-encontro-ubuntu-pt-sintra/"
+++
Todos os meses, numa quinta-feira, a comunidade Ubuntu Portugal reúne-se, em Sintra, desta vez no HopSin.

Vem, traz um amigo ou um familiar e vem conviver e partilhar experiências com o resto da comunidade portuguesa..

Mais Informações:
HopSin BrewPub
Av. do Atlântico 1 · Colares
O wi-fi é grátis.
https://www.openstreetmap.org/way/843569497