+++
type = "Event"
date = 2023-01-19
title = "Encontro Ubuntu-pt @ Aveiro"
tags = ['evento', 'cal-ubuntu-pt']
event_url = "https://loco.ubuntu.com/events/ubuntu-pt/4308-encontro-ubuntu-pt-aveiro/"
+++
Pela primeira vez a comunidade Ubuntu Portugal reúne-se em Aveiro.

Seremos acolhidos pelos nossos amigos do Grupo de Linux da Universidade de Aveiro (https://glua.ua.pt), que nos vão dar a conhecer melhor as suas actividades e a Universidade.

Palestra na Universidade de Aveiro às 19h, e social depois na Adega do Evaristo às 20h

Vem, traz um amigo ou um familiar e vem conviver e partilhar experiências com o resto da comunidade portuguesa...

Para facilitar as marcações na palestra e jantar pedimos que respondas ao seguinte questionário: https://bitpoll.de/poll/ubpt-190123/vote/ 

Se tiveres dúvidas contacta-nos na o nosso grupo de telegram (https://t.me/ubuntuptgeral), ou no matrix (https://matrix.to/#/#ubuntu-pt:matrix.org)



Universidade de Aveiro:
Department of Electronics, Telecommunications and Informatics, 
4, Avenida João Jacinto Magalhães, Glória, Glória and Vera Cruz, 
Aveiro, 
3810-193, 
Portugal
https://www.openstreetmap.org/way/145482891#map=19/40.63316/-8.65939