#!/usr/bin/env python3

from collections import defaultdict
import datetime
import json
import os
import re
from typing import Any, Dict, List, Optional

import requests
import yaml
import icalendar


def parse_config() -> Dict[str, Any]:
    "Parse calendar_importer_config.yaml and validates its dict structure."
    conf = yaml.safe_load(open("calendar_importer_config.yaml"))

    if not conf:
        raise RuntimeError("config.yaml is empty")

    if not conf.get("markdown_dir"):
        raise RuntimeError("markdown_dir conf is not present")

    if not conf.get("tags"):
        conf["tags"] = []
    if not hasattr(conf["tags"], "__iter__"):
        raise RuntimeError("tags conf is not a list")

    if not conf.get("calendars"):
        raise RuntimeError("calendars list is not present or empty")

    for calendar in conf["calendars"]:
        if not calendar.get("url"):
            raise RuntimeError("url is not present in calendar", calendar)
        if not calendar.get("name"):
            calendar["name"] = calendar["url"]
        if not calendar.get("tags"):
            calendar["tags"] = []

    if conf.get("ignore_before"):
        conf["ignore_before"] = datetime.datetime.strptime(
            conf["ignore_before"], "%Y-%m-%d"
        ).date()

    return conf


def download_calendar(name: str, url: str) -> str:
    "Download ical and return its plain-text content."
    print(f"Downloading calendar {name} ({url})")
    r = requests.get(url)
    r.raise_for_status()
    return r.content.decode("UTF-8")


class ParseICSException(Exception):
    pass


def parse_ics(ics: str) -> List[Dict[str, Any]]:
    events: List[Dict[str, Any]] = []
    try:
        cal = icalendar.Calendar.from_ical(ics)
    except ValueError as e:
        raise ParseICSException(e)
    for comp in cal.walk():
        if comp.name == "VEVENT":
            dtstartdate = (
                comp.get("dtstart").dt.date()
                if isinstance(comp.get("dtstart").dt, datetime.datetime)
                else comp.get("dtstart").dt
            )

            events.append(
                {
                    "title": str(comp.get("summary")).strip(),
                    "dtstart": dtstartdate,
                    "description": (
                        str(comp.get("summary"))
                        if comp.get("description") is None
                        else str(comp.get("description"))
                    ),
                    "url": str(comp.get("url")),
                }
            )

    print(f"Parsed {len(events)} events")
    return events


def merge_events(
    all_events: list[Dict[str, Any]],
    events: list[Dict[str, Any]],
    ignore_before: Optional[Any] = None,
) -> list[Dict[str, Any]]:
    merged_events: list[Dict[str, Any]] = [e for e in all_events]

    counters = defaultdict(int)

    for event in events:
        if ignore_before and event["dtstart"] < ignore_before:
            counters["ignored"] += 1
            continue

        # check for duplicate
        for e in merged_events:
            if (
                e["title"].lower() == event["title"].lower()
                and e["dtstart"] == event["dtstart"]
            ):
                e["tags"] = list(set(e["tags"] + event["tags"]))
                counters["updated"] += 1
                break
        else:
            merged_events.append(event)
            counters["new"] += 1

    print(f"Merged {counters['new']+counters['updated']} events. ({dict(counters)})")
    return merged_events


def write_markdown(dir, events):
    print(f"Writing {len(events)} markdown files")
    for event in events:
        safe_title = event["title"].replace(" ", "_")
        safe_title = re.sub("[^0-9a-zA-Z-_]*", "", safe_title)

        filename = f"{event['dtstart'].strftime('%Y-%m-%d')}-{safe_title}.md"
        with open(os.path.join(dir, filename), "w") as f:
            print(f"    Writing {filename}")
            try:
                f.write("+++\n")
                f.write('type = "Event"\n')
                f.write(f"date = {event['dtstart'].strftime('%Y-%m-%d')}\n")
                f.write(f"title = \"{event['title']}\"\n")
                f.write(f"tags = {conf['tags'] + calendar['tags']}\n")
                f.write(f"event_url = \"{event['url']}\"\n")
                f.write("+++\n")
                f.write(event["description"])
            except Exception as e:
                print(f"Error writing file {filename}: {e}")
                continue


if __name__ == "__main__":
    conf = parse_config()
    os.makedirs(conf["markdown_dir"], exist_ok=True)

    all_events = []

    for calendar in conf["calendars"]:
        try:
            ics = download_calendar(calendar["name"], calendar["url"])
            events = parse_ics(ics)
            if calendar.get("replace_to_https"):
                for e in events:
                    e["url"] = e["url"].replace("http://", "https://")
            all_events = merge_events(
                all_events, events, ignore_before=conf.get("ignore_before")
            )

        except requests.HTTPError as e:
            print(f"Error downloading calendar {calendar['name']}: {e}")
            continue
        except ParseICSException as e:
            print(f"Error parsing calendar {calendar['name']}: {e}")
            continue

    write_markdown(conf["markdown_dir"], all_events)
